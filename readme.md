# Simple React Redux Todo App.

*To run* 

    npm install
    npm start

- [Todo App](http://david-react-redux-todo.s3-website-us-east-1.amazonaws.com/)



### Tools used

- [React Docs](https://facebook.github.io/react/docs/hello-world.html)
- [React Bootstrap](https://react-bootstrap.github.io/components.html)