import React, {Component} from 'react';
import {Modal} from 'react-bootstrap';
//import './App.css';
import TodosList from './components/TodosList';
import TodoInput from './components/TodoInput';
import {connect} from 'react-redux';
import {addTodo,editTodo,saveTodo,deleteTodo,completedTodo} from './actions';

class App extends Component {
    onAddTodo = (text) => {
        this.props.dispatch(addTodo(text))
    }
    onSetEditTodo = (id, edit) => {
        this.props.dispatch(editTodo(id,{edit}))
    }
    onCompletedTodo = (id, completed) => {
        this.props.dispatch(completedTodo(id,{completed}))
    }
    onSaveEditTodo = (id, text) => {
        this.props.dispatch(saveTodo(id,{text}))
    }
    onDeleteTodo = (id) => {
        this.props.dispatch(deleteTodo(id))
    }
    componentDidMount() {
        this.todoForm.focusInput();
    }

    render() {
        const {todos} = this.props;

        return (
            <div>
                <div className="Main container">
                    <div className="static-modal">
                        <Modal.Dialog>
                            <span className="todo-title">todos</span>

                            <div id="scroll-wrap">

                                <TodoInput
                                    onAddTodo={this.onAddTodo}
                                    ref={form => this.todoForm = form}
                                />

                                <TodosList
                                todos={todos}
                                onSetEditTodo={this.onSetEditTodo}
                                onSaveEditTodo={this.onSaveEditTodo}
                                onDeleteTodo={this.onDeleteTodo}
                                onCompletedTodo={this.onCompletedTodo}
                                />

                            </div>

                            <Modal.Footer>@2018</Modal.Footer>

                        </Modal.Dialog>
                    </div>

                </div>
            </div>


        );
    }


}

function mapStatetoProps(state){
    return {
        todos:state.todos
    }

}
export default connect(mapStatetoProps)(App);
