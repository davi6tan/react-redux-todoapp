import {uniqueId} from "../actions";

const mockTodos = [
    {
        id: uniqueId(),
        text:'Watch Superbowl',
        edit: false,
        completed:false
    },
    {
        id: uniqueId(),
        text:'Buy Pizza',
        edit: false,
        completed:false
    },
    {
        id: uniqueId(),
        text:'Write This todo',
        edit: false,
        completed:true
    },
];

export default mockTodos;