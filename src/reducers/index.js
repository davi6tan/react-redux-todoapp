import mockTodos from './mocktodos';


export default function todos(state = {todos: mockTodos}, action) {
    const {type, payload} = action
    if (type === 'ADD_TODO') {
        return {
            todos: state.todos.concat(payload)
        }
    }
    if (type === 'EDIT_TODO') {
        return {
            todos: state.todos.map(todo => {
                if (todo.id === payload.id) {
                    return Object.assign({}, todo, payload.params)
                }
                return todo
            })
        }
    }
    if (type === 'SAVE_TODO') {
        return {
            todos: state.todos.map(todo => {
                if (todo.id === payload.id) {
                    return Object.assign({}, todo, payload.params, {edit: false})
                }
                return todo
            })
        }
    }

    if (type === 'COMPLETED_TODO') {
        return {
            todos: state.todos.map(todo => {
                console.log("completed")
                if (todo.id === payload.id) {
                    return Object.assign({}, todo, payload.params)
                }
                return todo
            })
        }
    }
    if (type === 'DELETE_TODO') {
        return {
            todos: state.todos.filter(todo => todo.id !== payload.id)

        }
    }
    return state
}