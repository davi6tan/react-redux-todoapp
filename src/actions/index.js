let _id = 1;


export function uniqueId(){
    return _id++;

}

export function addTodo(text){
    return {
        type:'ADD_TODO',
        payload:{
            id:uniqueId(),
            text,
            edit: false,
            completed:false
        }
    }
}
export function editTodo(id,params={}){
    return {
        type:'EDIT_TODO',
        payload:{
            id,
            params
        }
    }
}

export function completedTodo(id,params={}){
    return {
        type:'COMPLETED_TODO',
        payload:{
            id,
            params
        }
    }
}
export function saveTodo(id,params={}){
    return {
        type:'SAVE_TODO',
        payload:{
            id,
            params
        }
    }
}

export function deleteTodo(id){
    return {
        type:'DELETE_TODO',
        payload:{
            id
        }
    }
}

