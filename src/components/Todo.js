import React, {Component} from "react";

class Todo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            todotext: this.props.todo.text
        }
    }

    onSubmit = (e) => {
        e.preventDefault();
        const text = this.state.todotext.trim();
        this.props.onSaveEditTodo(this.props.todo.id, text)
    }

    render() {
        return (
            <div>
                <form onSubmit={this.onSubmit}>
                    <div id="todo-list">
                        <p>
                            {
                                this.props.todo.edit
                                    ? <input type="text"
                                             value={this.state.todotext}
                                             onChange={e => this.setState({todotext: e.target.value})}

                                    />
                                    : <label className={(this.props.todo.completed) ? "completed" : ""}
                                             onDoubleClick={e => this.props.onSetEditTodo(this.props.todo.id, true)}>
                                        {
                                            this.props.todo.text
                                        }
                                    </label>


                            }

                            <button
                                type="button"
                                className="btn btn-success btn-completed-list-item"
                                onClick={e => this.props.onCompletedTodo(this.props.todo.id, !this.props.todo.completed)}>

                                <i className="glyphicon  glyphicon-ok"/>

                            </button>

                            <button
                                type="button"
                                className="btn btn-danger btn-removable-list-item"
                                onClick={e => this.props.onDeleteTodo(this.props.todo.id, e)}>

                                <i className="glyphicon  glyphicon-trash"/>

                            </button>


                        </p>
                    </div>
                </form>
            </div>

        );
    }


}

export default Todo;