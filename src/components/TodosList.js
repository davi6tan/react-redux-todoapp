import React from "react";
import Todo from "./Todo";

const TodosList = props =>{

    return (
        <div>
            {
                props.todos.map(t => (
                        <Todo
                            key={t.id}
                            todo={t}
                            onSetEditTodo={props.onSetEditTodo}
                            onSaveEditTodo={props.onSaveEditTodo}
                            onDeleteTodo={props.onDeleteTodo}
                            onCompletedTodo={props.onCompletedTodo}
                        />
                    )
                )
            }

        </div>
    )


}

export default TodosList;