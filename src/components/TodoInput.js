import React, {Component} from "react";

//// main box top /////
class TodoInput extends Component {

    render() {
        return (
            <form onSubmit={this.onSubmit}>

                <div className="input-box">
                    <p>
                        <input type="text"
                               ref={input => this._todoText = input}
                               placeholder="What needs to be done? "
                        />
                    </p>
                </div>
            </form>


        )
    }

    focusInput() {
        this._todoText.focus();
    }

    onSubmit = (e) => {
        e.preventDefault();
        /// grab text
        const text = this._todoText.value.trim();
        if (text.length === 0) return; //end

        this._todoText.value = "";

        this.props.onAddTodo(text);
    }


}

export default TodoInput;